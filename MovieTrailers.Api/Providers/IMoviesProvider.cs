﻿using MovieTrailers.Api.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace MovieTrailers.Api.Providers
{
    public interface IMoviesProvider
    {
        /// <summary>
        /// Retrieves a list of movies based on a given expression
        /// </summary>
        /// <param name="expression">Expression for search (e.g "Leon The Professional" or "Inception 2010").</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <example>
        /// <code>
        /// var movies = _moviesProvider.GetMovies("inception (2010)", cancellationToken);
        /// </code>
        /// </example>
        Task<MovieResult> GetMovies(string expression, CancellationToken cancellationToken);
    }
}
