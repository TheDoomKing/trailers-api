﻿using MovieTrailers.Api.Contracts;
using MovieTrailers.Api.Http;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace MovieTrailers.Api.Providers
{
    public class ImdbProvider : IMoviesProvider
    {
        private readonly ImdbClient _client;

        public ImdbProvider(ImdbClient client)
        {
            _client = client;
        }

        /// <inheritdoc cref="IMoviesProvider.GetMovies(string, CancellationToken)"/>
        public async Task<MovieResult> GetMovies(string expression, CancellationToken cancellationToken)
        {
            var result = new MovieResult
            {
                Movies = new List<MovieData>()
            };
            
            var movieResult = await _client.SearchMovies(expression, cancellationToken);

            foreach (var item in movieResult.Results)
            {
                var trailer = await _client.GetTrailer(item.Id, cancellationToken);
                var data = new MovieData
                {
                    Description = item.Description,
                    Image = item.Image,
                    ResultType = item.ResultType,
                    Title = item.Title,
                    VideoUrl = trailer.VideoUrl
                };
                result.Movies.Add(data);
            }

            return result;
        }
    }
}
