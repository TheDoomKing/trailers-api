using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using MovieTrailers.Api.Configuration;
using MovieTrailers.Api.Contracts;
using MovieTrailers.Api.Health;
using MovieTrailers.Api.Http;
using MovieTrailers.Api.Middleware;
using MovieTrailers.Api.Providers;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text.Json;

namespace MovieTrailers.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var configuration = Configuration.GetSection(nameof(AppSettings)).Get<AppSettings>() ?? new AppSettings();

            // General
            services.AddControllers(options =>
            {
                options.CacheProfiles.Add("Default", new CacheProfile()
                {
                    Duration = configuration.Caching.DurationSeconds,
                    VaryByQueryKeys = new[] { "expression" }
                });
            });
            services.AddHttpClient<ImdbClient>(ImdbClient.Name, (serviceProvider, client) =>
            {
                var options = serviceProvider.GetRequiredService<IOptions<ImdbSettings>>();
                client.BaseAddress = new Uri(options.Value.BaseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            });
            if (configuration.Caching.Enabled)
            {
                services.AddResponseCaching();
            }

            // Swagger
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "MovieTrailers.Api",
                    Version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion
                });
            });

            // Movies
            services.AddTransient<IMoviesProvider, ImdbProvider>();

            // Health
            services.AddHealthChecks().AddCheck<Imdb>(nameof(Imdb));

            // Configuration
            services.Configure<AppSettings>(Configuration.GetSection(nameof(AppSettings)));
            services.Configure<ImdbSettings>(Configuration.GetSection(nameof(ImdbSettings)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IOptions<AppSettings> options)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(options => options.SwaggerEndpoint("v1/swagger.json", "MovieTrailers.Api"));
            }

            if (options.Value.Caching.Enabled)
            {
                app.UseResponseCaching();
            }
            app.UseRouting();
            app.UseAuthorization();
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseHealthChecks("/health", new HealthCheckOptions
            {
                ResponseWriter = async (context, report) =>
                {
                    context.Response.ContentType = "application/json";
                    var result = new HealthCheckResult
                    {
                        Status = report.Status.ToString(),
                        Checks = report.Entries.Select(x => new HealthCheck
                        {
                            Component = x.Key,
                            Status = x.Value.Status,
                            StatusDescription = x.Value.Status.ToString(),
                            Description = x.Value.Description,
                            Duration = x.Value.Duration
                        }),
                        Duration = report.TotalDuration
                    };
                    await context.Response.WriteAsync(JsonSerializer.Serialize(result));
                }
            });
        }
    }
}
