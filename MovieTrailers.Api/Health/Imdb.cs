﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using MovieTrailers.Api.Http;
using System.Threading;
using System.Threading.Tasks;

namespace MovieTrailers.Api.Health
{
    public class Imdb : IHealthCheck
    {
        private readonly ImdbClient _client;

        public Imdb(ImdbClient client)
        {
            _client = client;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            var available = await _client.CheckDestinationAvailableAsync(cancellationToken);
            return available ? HealthCheckResult.Healthy() : HealthCheckResult.Unhealthy();
        }
    }
}
