﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using MovieTrailers.Api.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieTrailers.Api.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                await HandleExceptionAsync(context);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context)
        {
            var result = new ErrorResult
            {
                Errors = new List<string> { "INTERNAL_SERVER_ERROR" }
            };

            context.Response.StatusCode = 500;
            context.Response.ContentType = "application/json";
            return context.Response.WriteAsync(SerializeObject(result));
        }

        private static string SerializeObject(object data)
        {
            return System.Text.Json.JsonSerializer.Serialize(data);
        }
    }
}
