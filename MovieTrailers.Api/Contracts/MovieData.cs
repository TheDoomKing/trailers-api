﻿using System.Text.Json.Serialization;

namespace MovieTrailers.Api.Contracts
{
    public class MovieData
    {
        [JsonPropertyName("resultType")]
        public string ResultType { get; set; }

        [JsonPropertyName("image")]
        public string Image { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("videoUrl")]
        public string VideoUrl { get; set; }
    }
}
