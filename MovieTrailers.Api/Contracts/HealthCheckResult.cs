﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;

namespace MovieTrailers.Api.Contracts
{
    public class HealthCheckResult
    {
        public string Status { get; set; }
        public IEnumerable<HealthCheck> Checks { get; set; }
        public TimeSpan Duration { get; set; }
    }

    public class HealthCheck
    {
        /// <summary>
        /// Gets the health status of the component that was checked.
        /// </summary>
        public HealthStatus Status { get; set; }
        /// <summary>
        /// Gets a human-readable description of the status of the component that was checked.
        /// </summary>
        public string StatusDescription { get; set; }

        /// <summary>
        /// Gets the name of the component / application part that was checked.
        /// </summary>
        public string Component { get; set; }

        /// <summary>
        /// Gets an additional description (if any) explaining the current state of the component.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets the health check execution duration.
        /// </summary>
        public TimeSpan Duration { get; set; }
    }
}
