﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MovieTrailers.Api.Contracts
{
    public class ErrorResult
    {
        /// <summary>
        /// List of result errors
        /// </summary>
        [JsonPropertyName("errors")]
        public List<string> Errors { get; set; }
    }
}
