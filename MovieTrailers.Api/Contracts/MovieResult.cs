﻿using System.Collections.Generic;

namespace MovieTrailers.Api.Contracts
{
    public class MovieResult
    {
        public List<MovieData> Movies { get; set; }
    }
}
