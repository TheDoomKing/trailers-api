﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MovieTrailers.Api.Configuration;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace MovieTrailers.Api.Http
{
    public class ImdbClient
    {
        public const string Name = "Imdb";
        private readonly IOptions<ImdbSettings> _settings;
        private readonly ILogger<ImdbClient> _logger;
        private readonly HttpClient _httpClient;

        public ImdbClient(
            IOptions<ImdbSettings> settings,
            ILogger<ImdbClient> logger,
            HttpClient httpClient
            )
        {
            _settings = settings;
            _logger = logger;
            _httpClient = httpClient;
        }

        /// <summary>
        /// Performs a simple check on whether IMDB is available or not by querying a "test" keyword on the IMDB API
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckDestinationAvailableAsync(CancellationToken cancellationToken)
        {
            using var response = await _httpClient.GetAsync($"SearchKeyword/{_settings.Value.ApiKey}/test", cancellationToken);
            return response.IsSuccessStatusCode;
        }

        public async Task<MovieResult> SearchMovies(string expression, CancellationToken cancellationToken)
        {
            try
            {
                using var response = await _httpClient.GetAsync($"SearchMovie/{_settings.Value.ApiKey}/{expression}", cancellationToken);
                var json = await response.Content.ReadAsStringAsync(cancellationToken);
                return JsonSerializer.Deserialize<MovieResult>(json);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        public async Task<MovieTrailer> GetTrailer(string imdbId, CancellationToken cancellationToken)
        {
            try
            {
                using var response = await _httpClient.GetAsync($"YouTubeTrailer/{_settings.Value.ApiKey}/{imdbId}", cancellationToken);
                var json = await response.Content.ReadAsStringAsync(cancellationToken);
                return JsonSerializer.Deserialize<MovieTrailer>(json);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }


        public class MovieTrailer
        {
            [JsonPropertyName("imDbId")]
            public string ImDbId { get; set; }

            [JsonPropertyName("title")]
            public string Title { get; set; }

            [JsonPropertyName("fullTitle")]
            public string FullTitle { get; set; }

            [JsonPropertyName("type")]
            public string Type { get; set; }

            [JsonPropertyName("year")]
            public string Year { get; set; }

            [JsonPropertyName("videoId")]
            public string VideoId { get; set; }

            [JsonPropertyName("videoUrl")]
            public string VideoUrl { get; set; }

            [JsonPropertyName("errorMessage")]
            public string ErrorMessage { get; set; }
        }

        public class MovieItem
        {
            [JsonPropertyName("id")]
            public string Id { get; set; }

            [JsonPropertyName("resultType")]
            public string ResultType { get; set; }

            [JsonPropertyName("image")]
            public string Image { get; set; }

            [JsonPropertyName("title")]
            public string Title { get; set; }

            [JsonPropertyName("description")]
            public string Description { get; set; }
        }

        public class MovieResult
        {
            [JsonPropertyName("searchType")]
            public string SearchType { get; set; }

            [JsonPropertyName("expression")]
            public string Expression { get; set; }

            [JsonPropertyName("results")]
            public List<MovieItem> Results { get; set; }

            [JsonPropertyName("errorMessage")]
            public string ErrorMessage { get; set; }
        }
    }
}
