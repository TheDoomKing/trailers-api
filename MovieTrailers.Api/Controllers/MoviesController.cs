﻿using Microsoft.AspNetCore.Mvc;
using MovieTrailers.Api.Contracts;
using MovieTrailers.Api.Providers;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading;
using System.Threading.Tasks;

namespace MovieTrailers.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MoviesController : ControllerBase
    {
        private readonly IMoviesProvider _moviesProvider;

        public MoviesController(IMoviesProvider moviesProvider)
        {
            _moviesProvider = moviesProvider;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        [SwaggerResponse(200, Type = typeof(MovieResult))]
        [SwaggerResponse(500, Type = typeof(ErrorResult))]
        [Produces(typeof(MovieResult))]
        public async Task<IActionResult> GetMovies([FromQuery] string expression, CancellationToken cancellationToken)
        {
            var movies = await _moviesProvider.GetMovies(expression, cancellationToken);
            return Ok(movies);
        }
    }
}
