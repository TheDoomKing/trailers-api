﻿namespace MovieTrailers.Api.Configuration
{
    public class ImdbSettings
    {
        public string BaseUrl { get; set; }
        public string ApiKey { get; set; }
    }
}
