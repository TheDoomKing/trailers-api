﻿namespace MovieTrailers.Api.Configuration
{
    public class AppSettings
    {
        public Caching Caching { get; set; } = new Caching();
    }

    public class Caching
    {
        public bool Enabled { get; set; } = true;
        public int DurationSeconds { get; set; } = 120;
    }
}
